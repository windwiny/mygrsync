#!/usr/bin/env python
#Boa:App:BoaApp

import sys
import locale
reload(sys)
sys.setdefaultencoding(locale.getdefaultlocale()[1])
del sys.setdefaultencoding

import wx

import mygrsyncframe

modules ={u'mygrsyncdlg': [0, '', u'mygrsyncdlg.py'],
 u'mygrsyncframe': [1, u'mygrsync', u'mygrsyncframe.py']}

class BoaApp(wx.App):
    def OnInit(self):
        self.main = mygrsyncframe.create(None)
        self.main.Show()
        self.SetTopWindow(self.main)
        return True

def main():
    application = BoaApp(0)
    application.MainLoop()

if __name__ == '__main__':
    main()
